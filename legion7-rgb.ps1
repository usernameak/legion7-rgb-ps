<#
.SYNOPSIS
  Script for controlling LED lights on Lenovo Legion 7
.DESCRIPTION
  Make sure that iCue is not running. No need to kill it, just exit the application by right clicking
  the icon in the tray and selecting Quit.
  Make sure that led-settings.ps1 is in your path.
  Run script
  legion7-rgb.ps1 [-all <hex color code>] [-logo <hex color code>] [-keys <hex color code>] [-vents <hex color code>] [-neon <hex color code>]
.PARAMETER <Parameter_Name>
  -keys <ffffff> (optional) sets color of all keyboard LEDs to ffffff
  -logo <ffffff> (optional) sets color of Legion logo LEDs to ffffff
  -vents <ffffff> (optional) sets color of vent LEDs to ffffff
  -neon <ffffff> (optional) sets color of neon (front and side rim) LEDs to ffffff
.INPUTS
  None
.OUTPUTS
  None
.NOTES
  Version:        1.0
  Author:         Peter Vazny
  Creation Date:  12/31/2021
  Purpose/Change: Initial script development

.EXAMPLE
  # Set keyboard to ff0000, logo to ffffff, vents to 00ff00 and turn off neon LEDs
  legion7-rgb.ps1 -keys ff0000 -logo ffffff -vents 00ff00 -neon 000000
  # turn off all LEDs
  legion7-rgb.ps1 -all 000000
#>



param(
    [Parameter(Mandatory = $false, HelpMessage = "Enter a valid color in hex, for example F0F0F0")]
    [ValidatePattern("^[A-Fa-f0-9]{6}$")] [string] $keys,
    [Parameter(Mandatory = $false, HelpMessage = "Enter a valid color in hex, for example F0F0F0")]
    [ValidatePattern("^[A-Fa-f0-9]{6}$")] [string] $logo,
    [Parameter(Mandatory = $false, HelpMessage = "Enter a valid color in hex, for example F0F0F0")]
    [ValidatePattern("^[A-Fa-f0-9]{6}$")] [string] $vents,
    [Parameter(Mandatory = $false, HelpMessage = "Enter a valid color in hex, for example F0F0F0")]
    [ValidatePattern("^[A-Fa-f0-9]{6}$")] [string] $neon,
    [Parameter(Mandatory = $false, HelpMessage = "Enter a valid color in hex, for example F0F0F0")]
    [ValidatePattern("^[A-Fa-f0-9]{6}$")] [string] $all
)


#---------------------------------------------------------[Initialisations]--------------------------------------------------------

. (".\led-settings.ps1")

Add-Type -AssemblyName System.Runtime.WindowsRuntime

[void][Windows.Foundation.IAsyncOperation`1,    Windows.Foundation,    ContentType=WindowsRuntime]
[void][Windows.Devices.Enumeration.DeviceInformation,Windows.Devices.Enumeration,ContentType = WindowsRuntime]
[void][Windows.Devices.HumanInterfaceDevice.HidDevice, Windows.Devices.HumanInterfaceDevice, ContentType = WindowsRuntime]
[void][Windows.Devices.Enumeration.DeviceInformationCollection, Windows.Devices.Enumeration, ContentType = WindowsRuntime]

$_taskMethods = [System.WindowsRuntimeSystemExtensions].GetMethods() | ? {
    $_.Name -eq 'AsTask' -and $_.GetParameters().Count -eq 1
}

$asTaskGeneric = ($_taskMethods | ? { $_.GetParameters()[0].ParameterType.Name -eq 'IAsyncOperation`1' })[0];

#-----------------------------------------------------------[Functions]------------------------------------------------------------

Function Await($WinRtTask, $ResultType) {
    $asTask = $asTaskGeneric.MakeGenericMethod($ResultType)
    $netTask = $asTask.Invoke($null, @($WinRtTask))
    $netTask.Wait(-1) | Out-Null
    $netTask.Result
}

Function ParseHexColor([string] $hexColor){
    $hexBytes = [byte[]] -split ($hexColor -replace '..', '0x$& ')
    return $hexBytes
}

Function GetDevice([uint16] $vendorId, [uint16] $productId, [uint16] $usagePage, [uint16] $usage){
    $selector = [Windows.Devices.HumanInterfaceDevice.HidDevice]::GetDeviceSelector($usagePage, $usage, $vendorId, $productId)
    $devices = Await ([Windows.Devices.Enumeration.DeviceInformation]::FindAllAsync($selector)) ([Windows.Devices.Enumeration.DeviceInformationCollection])

    if($devices){
        $device = Await ([Windows.Devices.HumanInterfaceDevice.HidDevice]::FromIdAsync($devices[0].Id, [Windows.Storage.FileAccessMode]::Read)) ([Windows.Devices.HumanInterfaceDevice.HidDevice])
        return $device
    }
}

Function SendFeatureReport([Windows.Devices.HumanInterfaceDevice.HidDevice] $device, [byte] $reportId, [byte] $instruction, [byte[]] $data, [uint16] $maxPacketSize = 192) {

    $chunkSize = $maxPacketSize - 4

    # Break into chunks
    $counter = [pscustomobject] @{ Value = 0 }
    $chunks = $data | Group-Object -Propert { [math]::Floor($counter.Value++ / $chunkSize)}

    foreach ($chunk in $chunks) {
        [byte]$numberOfGroups = [math]::Ceiling($chunk.Count / 4)

        [byte[]] $bytes = @( $reportId, $instruction, $numberOfGroups, 0x00 ) + $chunk.Group

        #pad chunk to max size with 0s
        if($bytes.Length -lt $maxPacketSize) {$bytes += (1..($maxPacketSize - $bytes.Length) | ForEach-Object {[byte]0})}

        $featureReport = $device.CreateFeatureReport()
        [Windows.Storage.Streams.IBuffer]$buffer = [System.Runtime.InteropServices.WindowsRuntime.WindowsRuntimeBufferExtensions]::AsBuffer($bytes)
        $featureReport.Data = $buffer
        $ret = Await ($device.SendFeatureReportAsync($featureReport)) ([uint32])
    }

}

Function GenerateRgbData([byte[]] $ledIds, [byte[]] $rgb){
    $data = 1..($ledIds.Length * 4) | ForEach-Object { [byte]0 }
    for($i=0;$i -lt $ledIds.Length;$i++){
        $j = $i * 4
        $data[$j] = $ledIds[$i]
        $data[++$j] = $rgb[0]
        $data[++$j] = $rgb[1]
        $data[++$j] = $rgb[2]
    }
    return $data
}


#-----------------------------------------------------------[Execution]------------------------------------------------------------


if($all) {
    $keys = $all
    $logo = $all
    $vents = $all
    $neon = $all
}

foreach ($legionDevice in $legion_devices) {
    $device = GetDevice $legionDevice.VendorId $legionDevice.ProductId $legionDevice.UsagePage $legionDevice.Usage
    if($device) {
        Write-Host "Found: $($legionDevice.Name)"
        #unlock controller for editing
        SendFeatureReport $device 0x07 0xb2 @(0x00)

        #change key colors
        if($keys){
            $ledGroup = $legionDevice.LedGroups["keys"]
            $ledData = GenerateRgbData $ledGroup.LedIds (parseHexColor $keys)
            SendFeatureReport $device 0x07 $ledGroup.Bank $ledData
        }

        #change key colors
        if($logo){
            $ledGroup = $legionDevice.LedGroups["logo"]
            $ledData = GenerateRgbData $ledGroup.LedIds (parseHexColor $logo)
            SendFeatureReport $device 0x07 $ledGroup.Bank $ledData
        }

        #change key colors
        if($vents){
            $ledGroup = $legionDevice.LedGroups["vents"]
            $ledData = GenerateRgbData $ledGroup.LedIds (parseHexColor $vents)
            SendFeatureReport $device 0x07 $ledGroup.Bank $ledData
        }

        #change key colors
        if($neon){
            $ledGroup = $legionDevice.LedGroups["neon"]
            $ledData = GenerateRgbData $ledGroup.LedIds (parseHexColor $neon)
            SendFeatureReport $device 0x07 $ledGroup.Bank $ledData
        }

    }
}


